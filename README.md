# ValueObjectGeneration

## Links
* [Project Lombok](https://projectlombok.org/)
* [AutoValue](https://github.com/google/auto/tree/master/value)
* [Immutables](http://immutables.github.io/)
* [Joda Beans](http://www.joda.org/joda-beans/)

## Project Lombok

[Since 2009](https://projectlombok.org/changelog)

[Introduction](https://www.baeldung.com/intro-to-project-lombok)

[Stable Features](https://projectlombok.org/features/all)

[Baeldung](https://www.baeldung.com/intro-to-project-lombok)


``` bash
gradle init --type java-application
```

[gradle settings](https://projectlombok.org/setup/gradle)

=> IntelliJ needs Plugin
[IntelliJ Setup](https://projectlombok.org/setup/intellij)

=> VS Code needs Plugin
[VS Code Plugin](https://marketplace.visualstudio.com/items?itemName=GabrielBB.vscode-lombok)

## AutoValue

[Detailed Documentation](https://github.com/google/auto/blob/master/value/userguide/index.md)

[Baeldung](https://www.baeldung.com/introduction-to-autovalue)

## Immutables

## Joda Beans
[User Guide - Code Generation](https://www.joda.org/joda-beans/userguide-codegen.html)

[Gradle Plugin](https://github.com/andreas-schilling/joda-beans-gradle-plugin)

[Example Project](https://github.com/andreas-schilling/joda-beans-test)

## Misc

[JUnit 5 with Gradle 4.6 and up](https://www.petrikainulainen.net/programming/testing/junit-5-tutorial-running-unit-tests-with-gradle/)

VS Code QuickFix Keyboard ShortCut remapping
https://stackoverflow.com/questions/34020340/how-to-open-the-lightbulb-via-shortcut
Default "ctrl"+"."
