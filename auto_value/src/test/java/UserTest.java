import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.UUID;

class UserTest {

    @Test
    void builderMissingValues() {
        assertThatThrownBy(() -> User.builder().build())
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Missing required properties: eMail id");
    }

    @Test
    void builderRequiredArgs() {
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = User.builder()
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(user).isNotNull();
        assertThat(user.name()).isNotPresent();
        assertThat(user.eMail()).isEqualTo(eMail);
        assertThat(user.id()).isEqualTo(id);
    }


    @Test
    void builderAllArgs() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = User.builder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(user).isNotNull();
        assertThat(user.name()).hasValue(name);
        assertThat(user.eMail()).isEqualTo(eMail);
        assertThat(user.id()).isEqualTo(id);
    }

    @Test
    void equalsWithSameData() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = User.builder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        User duplicate = User.builder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(duplicate).isEqualTo(user);
    }


    @Test
    void equalsWithDifferentData() {
        User user = User.builder()
                .name("John Doe")
                .eMail("john.doe@example.com")
                .id(UUID.randomUUID())
                .build();

        User otherUser = User.builder()
                .name("Jane Dae")
                .eMail("jane.dae@example.com")
                .id(UUID.randomUUID())
                .build();

        assertThat(otherUser).isNotEqualTo(user);
    }

    @Test
    void hashCodeWithSameData() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = User.builder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        User duplicate = User.builder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(duplicate.hashCode()).isEqualTo(user.hashCode());
    }

    @Test
    void hashCodeWithDifferentData() {
        User user = User.builder()
                .name("John Doe")
                .eMail("john.doe@example.com")
                .id(UUID.randomUUID())
                .build();

        User otherUser = User.builder()
                .name("Jane Dae")
                .eMail("jane.dae@example.com")
                .id(UUID.randomUUID())
                .build();

        assertThat(otherUser.hashCode()).isNotEqualTo(user.hashCode());
    }
}