import java.util.Optional;
import java.util.UUID;

import com.google.auto.value.AutoValue;

@AutoValue
abstract class User{
    abstract Optional<String> name();
    abstract String eMail();
    abstract UUID id();

    static Builder builder(){
        return new AutoValue_User.Builder();
    }

    @AutoValue.Builder
    abstract static class Builder {
        abstract Builder name(String name);
        abstract Builder eMail(String eMail);
        abstract Builder id(UUID id);
        abstract User build();
    }
}