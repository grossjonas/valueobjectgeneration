import java.util.UUID;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Value;

@Builder
@Value
class User{
    private final String name;
    private final @NonNull String eMail;
    private final @NonNull UUID id;
}