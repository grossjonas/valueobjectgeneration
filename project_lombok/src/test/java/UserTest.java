import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.UUID;

class UserTest {

    @Test
    void builderMissingValues() {
        assertThatThrownBy(() -> new User.UserBuilder().build())
                .isInstanceOf(NullPointerException.class)
                .hasMessageContaining("eMail is marked @NonNull but is null");
    }

    @Test
    void builderRequiredArgs() {
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = new User.UserBuilder()
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(user).isNotNull();
        assertThat(user.getName()).isNull();
        assertThat(user.getEMail()).isEqualTo(eMail);
        assertThat(user.getId()).isEqualTo(id);
    }

    @Test
    void builderAllArgs() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = new User.UserBuilder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(user).isNotNull();
        assertThat(user.getName()).isEqualTo(name);
        assertThat(user.getEMail()).isEqualTo(eMail);
        assertThat(user.getId()).isEqualTo(id);
    }

    @Test
    void equalsWithSameData() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = new User.UserBuilder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        User duplicate = new User.UserBuilder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(duplicate).isEqualTo(user);
    }

    @Test
    void equalsWithDifferentData() {
        User user = new User.UserBuilder()
                .name("John Doe")
                .eMail("john.doe@example.com")
                .id(UUID.randomUUID())
                .build();

        User otherUser = new User.UserBuilder()
                .name("Jane Dae")
                .eMail("jane.dae@example.com")
                .id(UUID.randomUUID())
                .build();

        assertThat(otherUser).isNotEqualTo(user);
    }

    @Test
    void hashCodeWithSameData() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = new User.UserBuilder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        User duplicate = new User.UserBuilder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(duplicate.hashCode()).isEqualTo(user.hashCode());
    }

    @Test
    void hashCodeWithDifferentData() {
        User user = new User.UserBuilder()
                .name("John Doe")
                .eMail("john.doe@example.com")
                .id(UUID.randomUUID())
                .build();

        User otherUser = new User.UserBuilder()
                .name("Jane Dae")
                .eMail("jane.dae@example.com")
                .id(UUID.randomUUID())
                .build();

        assertThat(otherUser.hashCode()).isNotEqualTo(user.hashCode());
    }
}
