import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


class UserBuilderWithLambdaTest {
    @Test
    void construction(){
        String name = "Joe";
        String eMail = "Joe@example.com";
        UUID id = UUID.randomUUID();

        UserBuilderWithLambda user = new UserBuilderWithLambda.UserBuilder()
                .with($ -> {
                    $.name = name;
                    $.eMail = eMail;
                    $.id = id;
                })
                .build();

        assertThat(user.getName()).isEqualTo(name);
        assertThat(user.geteMail()).isEqualTo(eMail);
        assertThat(user.getId()).isEqualTo(id);
    }
}