import java.util.Objects;
import java.util.UUID;
import java.util.function.Consumer;

public class UserBuilderWithLambda {
    private final String name;
    private final String eMail;
    private final UUID id;

    private UserBuilderWithLambda(String name, String eMail, UUID id){
        this.name = name;
        this.eMail = eMail;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String geteMail() {
        return eMail;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserBuilderWithLambda that = (UserBuilderWithLambda) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(eMail, that.eMail) &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, eMail, id);
    }

    public static class UserBuilder{
        public String name;
        public String eMail;
        public UUID id;

        public UserBuilder with(Consumer<UserBuilder> builderConsumer){
            builderConsumer.accept(this);
            return this;
        }

        public UserBuilderWithLambda build(){
            return new UserBuilderWithLambda(this.name, this.eMail, this.id);
        }
    }
}
