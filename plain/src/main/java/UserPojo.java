import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class UserPojo {
    private String name = null;
    private String eMail;
    private UUID id;

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }

    public void setName(String name){
        this.name = name;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPojo userPojo = (UserPojo) o;
        return Objects.equals(name, userPojo.name) &&
                Objects.equals(eMail, userPojo.eMail) &&
                Objects.equals(id, userPojo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, eMail, id);
    }

    @Override
    public String toString() {
        return "UserPojo{" +
                "name='" + name + '\'' +
                ", eMail='" + eMail + '\'' +
                ", id=" + id +
                '}';
    }
}
