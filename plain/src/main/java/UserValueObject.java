import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public final class UserValueObject {
    private final String name;
    private final String eMail;
    private final UUID id;

    public UserValueObject(String eMail, UUID id) {
        this.name = null;
        this.eMail = eMail;
        this.id = id;
    }

    public UserValueObject(String name, String eMail, UUID id) {
        this.name = name;
        this.eMail = eMail;
        this.id = id;
    }

    public Optional<String> getName() {
        return Optional.ofNullable(name);
    }

    public String geteMail() {
        return eMail;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserValueObject that = (UserValueObject) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(eMail, that.eMail) &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, eMail, id);
    }
}
