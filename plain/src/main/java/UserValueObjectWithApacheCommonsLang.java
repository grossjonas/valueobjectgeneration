import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Optional;
import java.util.UUID;

public class UserValueObjectWithApacheCommonsLang {
    private final Optional<String> name;
    private final String eMail;
    private final UUID id;

    public UserValueObjectWithApacheCommonsLang(Optional<String> name, String eMail, UUID id) {
        this.name = name;
        this.eMail = eMail;
        this.id = id;
    }

    public Optional<String> getName() {
        return name;
    }

    public String geteMail() {
        return eMail;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        UserValueObjectWithApacheCommonsLang that = (UserValueObjectWithApacheCommonsLang) o;

        return new EqualsBuilder()
                .append(name, that.name)
                .append(eMail, that.eMail)
                .append(id, that.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(eMail)
                .append(id)
                .toHashCode();
    }
}
