import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class UserTest {

    @Test
    void builderMissingValues() {
        assertThatThrownBy(() -> User.builder().build())
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Argument 'eMail' must not be null");
    }

    @Test
    void builderRequiredArgs() {
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = User.builder()
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(user).isNotNull();
        assertThat(user.getName()).isNull();
        assertThat(user.getEMail()).isEqualTo(eMail);
        assertThat(user.getId()).isEqualTo(id);
    }

    @Test
    void builderAllArgs() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = User.builder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(user).isNotNull();
        assertThat(user.getName()).isEqualTo(name);
        assertThat(user.getEMail()).isEqualTo(eMail);
        assertThat(user.getId()).isEqualTo(id);
    }

    @Test
    void equalsWithSameData() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = User.builder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        User duplicate = User.builder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(duplicate).isEqualTo(user);
    }

    @Test
    void equalsWithDifferentData() {
        User user = User.builder()
                .name("John Doe")
                .eMail("john.doe@example.com")
                .id(UUID.randomUUID())
                .build();

        User otherUser = User.builder()
                .name("Jane Dae")
                .eMail("jane.dae@example.com")
                .id(UUID.randomUUID())
                .build();

        assertThat(otherUser).isNotEqualTo(user);
    }

    @Test
    void hashCodeWithSameData() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = User.builder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        User duplicate = User.builder()
                .name(name)
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(duplicate.hashCode()).isEqualTo(user.hashCode());
    }

    @Test
    void hashCodeWithDifferentData() {
        User user = User.builder()
                .name("John Doe")
                .eMail("john.doe@example.com")
                .id(UUID.randomUUID())
                .build();

        User otherUser = User.builder()
                .name("Jane Dae")
                .eMail("jane.dae@example.com")
                .id(UUID.randomUUID())
                .build();

        assertThat(otherUser.hashCode()).isNotEqualTo(user.hashCode());
    }
}