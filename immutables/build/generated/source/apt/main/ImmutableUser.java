

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import javax.annotation.Generated;

/**
 * Immutable implementation of {@link User}.
 * <p>
 * Use the builder to create immutable instances:
 * {@code ImmutableUser.builder()}.
 */
@SuppressWarnings({"all"})
@Generated("org.immutables.processor.ProxyProcessor")
@org.immutables.value.Generated(from = "User", generator = "Immutables")
public final class ImmutableUser implements User {
  private final String name;
  private final String eMail;
  private final UUID id;

  private ImmutableUser(String name, String eMail, UUID id) {
    this.name = name;
    this.eMail = eMail;
    this.id = id;
  }

  /**
   * @return The value of the {@code name} attribute
   */
  @Override
  public Optional<String> name() {
    return Optional.ofNullable(name);
  }

  /**
   * @return The value of the {@code eMail} attribute
   */
  @Override
  public String eMail() {
    return eMail;
  }

  /**
   * @return The value of the {@code id} attribute
   */
  @Override
  public UUID id() {
    return id;
  }

  /**
   * Copy the current immutable object by setting a <i>present</i> value for the optional {@link User#name() name} attribute.
   * @param value The value for name
   * @return A modified copy of {@code this} object
   */
  public final ImmutableUser withName(String value) {
    String newValue = Objects.requireNonNull(value, "name");
    if (Objects.equals(this.name, newValue)) return this;
    return new ImmutableUser(newValue, this.eMail, this.id);
  }

  /**
   * Copy the current immutable object by setting an optional value for the {@link User#name() name} attribute.
   * An equality check is used on inner nullable value to prevent copying of the same value by returning {@code this}.
   * @param optional A value for name
   * @return A modified copy of {@code this} object
   */
  public final ImmutableUser withName(Optional<String> optional) {
    String value = optional.orElse(null);
    if (Objects.equals(this.name, value)) return this;
    return new ImmutableUser(value, this.eMail, this.id);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link User#eMail() eMail} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for eMail
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableUser withEMail(String value) {
    if (this.eMail.equals(value)) return this;
    String newValue = Objects.requireNonNull(value, "eMail");
    return new ImmutableUser(this.name, newValue, this.id);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link User#id() id} attribute.
   * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for id
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableUser withId(UUID value) {
    if (this.id == value) return this;
    UUID newValue = Objects.requireNonNull(value, "id");
    return new ImmutableUser(this.name, this.eMail, newValue);
  }

  /**
   * This instance is equal to all instances of {@code ImmutableUser} that have equal attribute values.
   * @return {@code true} if {@code this} is equal to {@code another} instance
   */
  @Override
  public boolean equals(Object another) {
    if (this == another) return true;
    return another instanceof ImmutableUser
        && equalTo((ImmutableUser) another);
  }

  private boolean equalTo(ImmutableUser another) {
    return Objects.equals(name, another.name)
        && eMail.equals(another.eMail)
        && id.equals(another.id);
  }

  /**
   * Computes a hash code from attributes: {@code name}, {@code eMail}, {@code id}.
   * @return hashCode value
   */
  @Override
  public int hashCode() {
    int h = 5381;
    h += (h << 5) + Objects.hashCode(name);
    h += (h << 5) + eMail.hashCode();
    h += (h << 5) + id.hashCode();
    return h;
  }

  /**
   * Prints the immutable value {@code User} with attribute values.
   * @return A string representation of the value
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder("User{");
    if (name != null) {
      builder.append("name=").append(name);
    }
    if (builder.length() > 5) builder.append(", ");
    builder.append("eMail=").append(eMail);
    builder.append(", ");
    builder.append("id=").append(id);
    return builder.append("}").toString();
  }

  /**
   * Creates an immutable copy of a {@link User} value.
   * Uses accessors to get values to initialize the new immutable instance.
   * If an instance is already immutable, it is returned as is.
   * @param instance The instance to copy
   * @return A copied immutable User instance
   */
  public static ImmutableUser copyOf(User instance) {
    if (instance instanceof ImmutableUser) {
      return (ImmutableUser) instance;
    }
    return ((ImmutableUser.Builder) ImmutableUser.builder())
        .name(instance.name())
        .eMail(instance.eMail())
        .id(instance.id())
        .build();
  }

  /**
   * Creates a builder for {@link ImmutableUser ImmutableUser}.
   * @return A new ImmutableUser builder
   */
  public static EMailBuildStage builder() {
    return new ImmutableUser.Builder();
  }

  /**
   * Builds instances of type {@link ImmutableUser ImmutableUser}.
   * Initialize attributes and then invoke the {@link #build()} method to create an
   * immutable instance.
   * <p><em>{@code Builder} is not thread-safe and generally should not be stored in a field or collection,
   * but instead used immediately to create instances.</em>
   */
  @org.immutables.value.Generated(from = "User", generator = "Immutables")
  public static final class Builder implements EMailBuildStage, IdBuildStage, BuildFinal {
    private static final long INIT_BIT_E_MAIL = 0x1L;
    private static final long INIT_BIT_ID = 0x2L;
    private static final long OPT_BIT_NAME = 0x1L;
    private long initBits = 0x3L;
    private long optBits;

    private String name;
    private String eMail;
    private UUID id;

    private Builder() {
    }

    /**
     * Initializes the optional value {@link User#name() name} to name.
     * @param name The value for name
     * @return {@code this} builder for chained invocation
     */
    public final Builder name(String name) {
      checkNotIsSet(nameIsSet(), "name");
      this.name = Objects.requireNonNull(name, "name");
      optBits |= OPT_BIT_NAME;
      return this;
    }

    /**
     * Initializes the optional value {@link User#name() name} to name.
     * @param name The value for name
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder name(Optional<String> name) {
      checkNotIsSet(nameIsSet(), "name");
      this.name = name.orElse(null);
      optBits |= OPT_BIT_NAME;
      return this;
    }

    /**
     * Initializes the value for the {@link User#eMail() eMail} attribute.
     * @param eMail The value for eMail 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder eMail(String eMail) {
      checkNotIsSet(eMailIsSet(), "eMail");
      this.eMail = Objects.requireNonNull(eMail, "eMail");
      initBits &= ~INIT_BIT_E_MAIL;
      return this;
    }

    /**
     * Initializes the value for the {@link User#id() id} attribute.
     * @param id The value for id 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder id(UUID id) {
      checkNotIsSet(idIsSet(), "id");
      this.id = Objects.requireNonNull(id, "id");
      initBits &= ~INIT_BIT_ID;
      return this;
    }

    /**
     * Builds a new {@link ImmutableUser ImmutableUser}.
     * @return An immutable instance of User
     * @throws java.lang.IllegalStateException if any required attributes are missing
     */
    public ImmutableUser build() {
      checkRequiredAttributes();
      return new ImmutableUser(name, eMail, id);
    }

    private boolean nameIsSet() {
      return (optBits & OPT_BIT_NAME) != 0;
    }

    private boolean eMailIsSet() {
      return (initBits & INIT_BIT_E_MAIL) == 0;
    }

    private boolean idIsSet() {
      return (initBits & INIT_BIT_ID) == 0;
    }

    private static void checkNotIsSet(boolean isSet, String name) {
      if (isSet) throw new IllegalStateException("Builder of User is strict, attribute is already set: ".concat(name));
    }

    private void checkRequiredAttributes() {
      if (initBits != 0) {
        throw new IllegalStateException(formatRequiredAttributesMessage());
      }
    }

    private String formatRequiredAttributesMessage() {
      List<String> attributes = new ArrayList<>();
      if (!eMailIsSet()) attributes.add("eMail");
      if (!idIsSet()) attributes.add("id");
      return "Cannot build User, some of required attributes are not set " + attributes;
    }
  }

  public interface EMailBuildStage {
    /**
     * Initializes the value for the {@link User#eMail() eMail} attribute.
     * @param eMail The value for eMail 
     * @return {@code this} builder for use in a chained invocation
     */
    IdBuildStage eMail(String eMail);
  }

  public interface IdBuildStage {
    /**
     * Initializes the value for the {@link User#id() id} attribute.
     * @param id The value for id 
     * @return {@code this} builder for use in a chained invocation
     */
    BuildFinal id(UUID id);
  }

  public interface BuildFinal {

    /**
     * Initializes the optional value {@link User#name() name} to name.
     * @param name The value for name
     * @return {@code this} builder for chained invocation
     */
    BuildFinal name(String name);

    /**
     * Initializes the optional value {@link User#name() name} to name.
     * @param name The value for name
     * @return {@code this} builder for use in a chained invocation
     */
    BuildFinal name(Optional<String> name);

    /**
     * Builds a new {@link ImmutableUser ImmutableUser}.
     * @return An immutable instance of User
     * @throws java.lang.IllegalStateException if any required attributes are missing
     */
    ImmutableUser build();
  }
}
