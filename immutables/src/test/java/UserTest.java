import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class UserTest {

    @Test
    void builderMissingValues() {
        // Cannot fail with staged Builder
        ImmutableUser.EMailBuildStage eMailBuildStage = ImmutableUser.builder();
        ImmutableUser.IdBuildStage idBuildStage = eMailBuildStage.eMail("someEmail");
        ImmutableUser.BuildFinal finalStage = idBuildStage.id(UUID.randomUUID());
        ImmutableUser user = finalStage.build();

        assertThat(user).isNotNull();
    }

    @Test
    void builderRequiredArgs() {
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = ImmutableUser.builder()
                .eMail(eMail)
                .id(id)
                .build();

        assertThat(user).isNotNull();
        assertThat(user.name()).isNotPresent();
        assertThat(user.eMail()).isEqualTo(eMail);
        assertThat(user.id()).isEqualTo(id);
    }

    @Test
    void builderAllArgs() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = ImmutableUser.builder()
                .eMail(eMail)
                .id(id)
                .name(name)
                .build();

        assertThat(user).isNotNull();
        assertThat(user.name()).isPresent().hasValue(name);
        assertThat(user.eMail()).isEqualTo(eMail);
        assertThat(user.id()).isEqualTo(id);
    }

    @Test
    void equalsWithSameData() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = ImmutableUser.builder()
                .eMail(eMail)
                .id(id)
                .name(name)
                .build();

        User duplicate = ImmutableUser.builder()
                .eMail(eMail)
                .id(id)
                .name(name)
                .build();

        assertThat(duplicate).isEqualTo(user);
    }

    @Test
    void equalsWithDifferentData() {
        User user = ImmutableUser.builder()
                .eMail("john.doe@example.com")
                .id(UUID.randomUUID())
                .name("John Doe")
                .build();

        User otherUser = ImmutableUser.builder()
                .eMail("jane.dae@example.com")
                .id(UUID.randomUUID())
                .name("Jane Dae")
                .build();

        assertThat(otherUser).isNotEqualTo(user);
    }

    @Test
    void hashCodeWithSameData() {
        String name = "John Doe";
        String eMail = "john.doe@example.com";
        UUID id = UUID.randomUUID();

        User user = ImmutableUser.builder()
                .eMail(eMail)
                .id(id)
                .name(name)
                .build();

        User duplicate = ImmutableUser.builder()
                .eMail(eMail)
                .id(id)
                .name(name)
                .build();

        assertThat(duplicate.hashCode()).isEqualTo(user.hashCode());
    }

    @Test
    void hashCodeWithDifferentData() {
        User user = ImmutableUser.builder()
                .eMail("john.doe@example.com")
                .id(UUID.randomUUID())
                .name("John Doe")
                .build();

        User otherUser = ImmutableUser.builder()
                .eMail("jane.dae@example.com")
                .id(UUID.randomUUID())
                .name("Jane Dae")
                .build();

        assertThat(otherUser.hashCode()).isNotEqualTo(user.hashCode());
    }

}