import org.immutables.value.Value;

import java.util.Optional;
import java.util.UUID;

@Value.Style(stagedBuilder = true)
@Value.Immutable
public interface User {
    Optional<String> name();
    String eMail();
    UUID id();
}
